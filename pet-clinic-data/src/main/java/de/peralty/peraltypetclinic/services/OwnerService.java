package de.peralty.peraltypetclinic.services;

import de.peralty.peraltypetclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {

    Owner findByLastName(String lastName);

}
