package de.peralty.peraltypetclinic.services;

import java.util.Set;

/***
 * Eigene Implementierung eines CRUD Services. Wie ein Spring Data Repository.
 * Java Generics werden für dieses Interface verwendet.
 * @param <T>
 * @param <ID>
 */
public interface CrudService<T, ID> {

    Set<T> findAll();

    T findById(ID id);

    T save(T object);

    void delete(T object);

    void deleteById(ID id);

}
