package de.peralty.peraltypetclinic.services;

import de.peralty.peraltypetclinic.model.Pet;

import java.util.Set;

public interface PetService extends CrudService<Pet, Long> {
}
