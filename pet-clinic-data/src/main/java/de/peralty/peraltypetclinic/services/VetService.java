package de.peralty.peraltypetclinic.services;

import de.peralty.peraltypetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {
}
